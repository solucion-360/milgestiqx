<?php

/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

require_once dirname(dirname(__FILE__)) . '/classes/OgoneCw/PaymentMethod.php'; 

class OgoneCw_CarteBleue extends OgoneCw_PaymentMethod
{
	public $machineName = 'cartebleue';
	public $admin_title = 'Carte Bleue';
	public $title = 'Carte Bleue';
	
	protected function getMethodSettings(){
		return array(
			'capturing' => array(
				'title' => __("Capturing", 'woocommerce_ogonecw'),
 				'default' => 'direct',
 				'description' => __("Should the amount be captured automatically after the order (direct) or should the amount only be reserved (deferred)?", 'woocommerce_ogonecw'),
 				'cwType' => 'select',
 				'type' => 'select',
 				'options' => array(
					'direct' => __("Directly after order", 'woocommerce_ogonecw'),
 					'deferred' => __("Deferred", 'woocommerce_ogonecw'),
 				),
 			),
 			'status_authorized' => array(
				'title' => __("Authorized Status", 'woocommerce_ogonecw'),
 				'default' => (defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.2.0') >= 0) ? 'wc-processing' : 'processing',
 				'description' => __("This status is set, when the payment was successfull and it is authorized.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
					'use-default' => __("Use WooCommerce rules", 'woocommerce_ogonecw'),
 				),
 				'is_order_status' => true,
 			),
 			'status_uncertain' => array(
				'title' => __("Uncertain Status", 'woocommerce_ogonecw'),
 				'default' => (defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.2.0') >= 0) ? 'wc-on-hold' : 'on-hold',
 				'description' => __("You can specify the order status for new orders that have an uncertain authorisation status.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
				),
 				'is_order_status' => true,
 			),
 			'status_cancelled' => array(
				'title' => __("Cancelled Status", 'woocommerce_ogonecw'),
 				'default' => (defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.2.0') >= 0) ? 'wc-cancelled' : 'cancelled',
 				'description' => __("You can specify the order status when an order is cancelled.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
					'no_status_change' => __("Don't change order status", 'woocommerce_ogonecw'),
 				),
 				'is_order_status' => true,
 			),
 			'status_captured' => array(
				'title' => __("Captured Status", 'woocommerce_ogonecw'),
 				'default' => 'no_status_change',
 				'description' => __("You can specify the order status for orders that are captured either directly after the order or manually in the backend.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
					'no_status_change' => __("Don't change order status", 'woocommerce_ogonecw'),
 				),
 				'is_order_status' => true,
 			),
 			'status_success_after_uncertain' => array(
				'title' => __("HTTP Status for Successful Payments", 'woocommerce_ogonecw'),
 				'default' => 'no_status_change',
 				'description' => __("You can specify the order status for orders that are successful after being in a uncertain state. In order to use this setting, you will need to activate the http-request for status changes as outlined in the manual.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
					'no_status_change' => __("Don't change order status", 'woocommerce_ogonecw'),
 				),
 				'is_order_status' => true,
 			),
 			'status_refused_after_uncertain' => array(
				'title' => __("HTTP Status for Refused Payments", 'woocommerce_ogonecw'),
 				'default' => 'no_status_change',
 				'description' => __("You can specify the order status for orders that are refused after being in a uncertain state. In order to use this feature you will have to set up the http request for status changes as outlined in the manual.", 'woocommerce_ogonecw'),
 				'cwType' => 'orderstatusselect',
 				'type' => 'select',
 				'options' => array(
					'no_status_change' => __("Don't change order status", 'woocommerce_ogonecw'),
 				),
 				'is_order_status' => true,
 			),
 			'refusing_threshold' => array(
				'title' => __("Refused Transaction Threshold", 'woocommerce_ogonecw'),
 				'default' => '3',
 				'description' => __("A typical pattern of a fraud transaction is a series of refused transaction before one of them is accepted. This setting defines the threshold after any following transaction is marked as uncertain. E.g. a threshold of three will mark any successful transaction after three refused transaction as uncertain.", 'woocommerce_ogonecw'),
 				'cwType' => 'textfield',
 				'type' => 'text',
 			),
 			'threeds_state' => array(
				'title' => __("3D Secure", 'woocommerce_ogonecw'),
 				'default' => 'enabled',
 				'description' => __("With this setting you can disable a 3D secure check for this payment method. This setting only takes effect with Ajax Authorization (Flex Checkout) or Hidden Authorization (Alias Gateway).", 'woocommerce_ogonecw'),
 				'cwType' => 'select',
 				'type' => 'select',
 				'options' => array(
					'enabled' => __("Enable 3D secure check", 'woocommerce_ogonecw'),
 					'disabled' => __("Disable 3D secure check", 'woocommerce_ogonecw'),
 				),
 			),
 			'three_d_secure_behavior' => array(
				'title' => __("Non 3D Secure Behavior", 'woocommerce_ogonecw'),
 				'default' => 'never',
 				'description' => __("Some cards are not enrolled for the 3D secure process and you may exclude some cards from performing a 3D secure authorization. This setting controls, what should happend with these transactions. This setting requires that the parameter 'CCCTY' is returned.", 'woocommerce_ogonecw'),
 				'cwType' => 'select',
 				'type' => 'select',
 				'options' => array(
					'always' => __("Mark all transactions as uncertain, except those from countries listed below.", 'woocommerce_ogonecw'),
 					'never' => __("Do never mark the transaction as uncertain, except those from countries listed below.", 'woocommerce_ogonecw'),
 				),
 			),
 			'three_d_secure_country_list' => array(
				'title' => __("Non 3D Secure Behavior Country List", 'woocommerce_ogonecw'),
 				'default' => '',
 				'description' => __("The countries listed in this field are used to mark transaction as uncertain. The behavior depends on the setting above 'Non 3D Secure Behavior'. The country code is taken from the card. You need to make sure you return the parameter 'CCCTY'. As indicated by Ogone this value is only for 94% of the transaction correct. The list below must be a comma separated list of country codes. The codes must be in ISO 3166-2 format (e.g. DE,IT,FR).", 'woocommerce_ogonecw'),
 				'cwType' => 'textfield',
 				'type' => 'text',
 			),
 			'country_check' => array(
				'title' => __("Country Check", 'woocommerce_ogonecw'),
 				'default' => 'inactive',
 				'description' => __("The module can perform a check of the country code provided by the issuer of the card, the IP address country and the billing address country. In case they do not match, the transaction is marked as uncertain. This setting does not override any other rule for marking transaction as uncertain.", 'woocommerce_ogonecw'),
 				'cwType' => 'select',
 				'type' => 'select',
 				'options' => array(
					'inactive' => __("Inactive", 'woocommerce_ogonecw'),
 					'all' => __("All country codes must match.", 'woocommerce_ogonecw'),
 					'ip_country_code_issuer_code' => __("IP country code and issuer country code must match.", 'woocommerce_ogonecw'),
 					'ip_country_code_billing_code' => __("IP country and billing country code must match.", 'woocommerce_ogonecw'),
 					'issuer_code_billing_code' => __("Issuer country code and billing country code.", 'woocommerce_ogonecw'),
 				),
 			),
 			'authorizationMethod' => array(
				'title' => __("Authorization Method", 'woocommerce_ogonecw'),
 				'default' => 'PaymentPage',
 				'description' => __("Select the authorization method to use for processing this payment method.", 'woocommerce_ogonecw'),
 				'cwType' => 'select',
 				'type' => 'select',
 				'options' => array(
					'PaymentPage' => __("Payment Page", 'woocommerce_ogonecw'),
 					'HiddenAuthorization' => __("Hidden Authorization (Alias Gateway)", 'woocommerce_ogonecw'),
 					'AjaxAuthorization' => __("Ajax Authorization (Flex Checkout)", 'woocommerce_ogonecw'),
 				),
 			),
 		); 
	}
	
	public function __construct() {
		$this->icon = apply_filters(
			'woocommerce_ogonecw_cartebleue_icon', 
			OgoneCw_Util::getResourcesUrl('icons/cartebleue.png')
		);
		parent::__construct();
	}
	
	public function createMethodFormFields() {
		$formFields = parent::createMethodFormFields();
		
		return array_merge(
			$formFields,
			$this->getMethodSettings()
		);
	}

}