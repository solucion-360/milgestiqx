var ogoneFlexCheckout =  new function () {
	
	this.includeCss = function(url){
		var cssId = 'ogone-overlay';
		if (!document.getElementById(cssId))
		{
		    var head  = document.getElementsByTagName('head')[0];
		    var link  = document.createElement('link');
		    link.id   = cssId;
		    link.rel  = 'stylesheet';
		    link.type = 'text/css';
		    link.href = url;
		    link.media = 'all';
		    head.appendChild(link);
		}
	}
	
	this.createIframe = function(url, jQ){
		var over = 
			'<div id="ogone-flex-overlay" class="ogone-flex-overlay">'+
				
					'<iframe src="'+url+'" class="ogone-flex-content"></iframe>'+
			'</div>';
		jQ(over).appendTo('body');
		jQ('body').addClass('ogone-flex-noscroll');
	}

}