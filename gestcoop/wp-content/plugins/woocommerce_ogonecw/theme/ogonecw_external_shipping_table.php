
<div class="ogonecw-external-checkout-shipping">
	<?php if (!empty($errorMessage)): ?>
		<p class="payment-error woocommerce-error">
			<?php print $errorMessage; ?>
		</p>
	<?php endif; ?>
	<h3><?php echo __("Shipping Option", "woocommerce_ogonecw")?></h3>
	<table class="ogonecw-external-checkout-shipping-table">
	
	<?php 
	echo $rows;
	?>
	
	</table>
	<input type="submit" class="ogonecw-external-checkout-shipping-method-save-btn button btn btn-success ogonecw-external-checkout-button" name="save" value="<?php echo __("Save Shipping Method", "woocommerce_ogonecw"); ?>" data-loading-text="<?php echo __("Processing...", "woocommerce_ogonecw"); ?>">
	
	
	<script type="text/javascript">
	jQuery(function(){
		jQuery('.ogonecw-external-checkout-shipping-method-save-btn').hide();
	
		
		jQuery('.ogonecw-external-checkout-shipping-table  input:radio').on('change', function(){
					jQuery('.ogonecw-external-checkout-shipping-method-save-btn').click();
				
		});
	});
	</script>
</div>
