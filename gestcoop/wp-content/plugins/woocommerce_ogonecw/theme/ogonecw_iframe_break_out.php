<?php
add_filter( 'run_wptexturize', '__return_false' );
?>
<div class="woocommerce ogonecw">
	<?php echo __('Redirecting... Please Wait ', 'woocommerce_ogonecw'); ?>
	<script type="text/javascript"> 
		top.location.href = '<?php echo $url; ?>';
	</script>
	

	<noscript>
		<a class="button btn btn-success ogonecw-continue-button" href="<?php echo $url; ?>" target="_top"><?php echo __('If you are not redirected shortly, click here.', 'woocommerce_ogonecw'); ?></a>
	</noscript>
</div>