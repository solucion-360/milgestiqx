<?php
if ( ! defined( 'ABSPATH' ) ) {	exit; }

show_admin_bar(false);

add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2 );
function woo_change_order_received_text( $str, $order ) {
    return 'Gracias por tu pedido. Has recibido una copia de tu pedido en tu dirección email. Nos pondremos en contacto contigo para tramitar tu gestión.';
}

add_filter( 'woocommerce_checkout_fields' , 'edit_fields' );
function edit_fields($fields) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_country']);
	// unset($fields['billing']['billing_postcode']);
	// unset($fields['billing']['billing_address_1']);
	// unset($fields['billing']['billing_address_2']);
	// unset($fields['billing']['billing_city']);
	// unset($fields['billing']['billing_state']);
	$fields['order']['order_comments']['placeholder'] = 'Si tienes que darnos más información, ejemplo: número de factura, proforma...';
	$fields['billing']['billing_dni'] = array(
		'type'			=> 'text',
		'label'     	=> __('DNI', 'woocommerce'),
		'placeholder'   => _x('DNI', 'placeholder', 'woocommerce'),
		'required'  	=> true,
		'class'     	=> array('form-row-wide'),
		'clear'     	=> true,
		'priority'		=> 20
	);
	$fields['billing']['billing_cooperativa'] = array(
		'type'			=> 'select',
		'label'     	=> __('Selecciona la cooperativa a la que perteneces', 'woocommerce'),
		'placeholder'   => _x('Selecciona tu cooperativa', 'placeholder', 'woocommerce'),
		'required'  	=> true,
		'class'     	=> array('form-row-wide'),
		'clear'     	=> true,
		'priority'		=> 0,
		'options'       => array(
	    	'CoopArt'	=> __( 'CoopArt', 'wps' ),
	        'Tecnicoo'	=> __( 'Tecnicoo', 'wps' ),     
	        'Autonomoo' 	=> __( 'Autonomoo', 'wps' ),
	        'Factoo'		=> __( 'Factoo', 'wps' )
	    )
	);
    return $fields;
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'muestra_fields', 10, 1 );
function muestra_fields($order){
    echo '<p><strong>'.__('DNI').':</strong> ' . get_post_meta( $order->get_id(), '_billing_dni', true ) . '</p>';
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'muestra_fields_coop', 10, 1 );
function muestra_fields_coop($order){
    echo '<p><strong>'.__('Cooperativa').':</strong> ' . get_post_meta( $order->get_id(), '_billing_cooperativa', true ) . '</p>';
}

add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); 
function woo_custom_order_button_text() {
    return __( 'Pagar Pedido', 'woocommerce' ); 
}

add_filter('woocommerce_account_menu_items', 'custom_my_account_menu_items');
function custom_my_account_menu_items($items) {
    unset($items['downloads']);
    return $items;
}

add_filter( 'manage_edit-shop_order_columns', 'custom_shop_order_column', 20 );
function custom_shop_order_column($columns) {
    $reordered_columns = array();
    foreach( $columns as $key => $column){
        $reordered_columns[$key] = $column;
        if( $key ==  'order_status' ){
            $reordered_columns['cooperativa'] = __( 'Cooperativa','theme_domain');
        }
    }
    return $reordered_columns;
}

add_action( 'manage_shop_order_posts_custom_column' , 'custom_orders_list_column_content', 20, 2 );
function custom_orders_list_column_content( $column, $post_id ){
    switch ($column) {
        case 'cooperativa' :
            $cooperativa = get_post_meta( $post_id, '_billing_cooperativa', true );
            if(!empty($cooperativa))
                echo $cooperativa;
            else
                echo '<small>(<em>No hay valor</em>)</small>';
            break;
    }
}
add_filter( "manage_edit-shop_order_sortable_columns", 'custom_woo_admin_sort' );
function custom_woo_admin_sort( $columns ) {
    $custom = array(
        'cooperativa'    => '_billing_cooperativa',
    );
    return wp_parse_args( $custom, $columns );
}

add_filter( 'woocommerce_order_number', 'change_woocommerce_order_number', 10, 2);
function change_woocommerce_order_number( $order_id ) {
	$cooperativa = get_post_meta($order_id, '_billing_cooperativa', true );
	$prefix = '';
	switch ($cooperativa) {
	    case 'Factoo':
	        $prefix = 'fac-';
	        break;
	    case 'Tecnicoo':
	        $prefix = 'tec-';
	        break;
	    case 'CoopArt':
	        $prefix = 'art-';
	        break;
	    case 'Autonomoo':
	        $prefix = 'aut-';
	        break;
	    default:
	    	$prefix = '';
	        break;
	}
    $new_order_id = $prefix . $order_id;
    return $new_order_id;
}

