{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="container">
  <div class="row col-md-12 section-logos">
	  <h2>Empresas que recomienda <img class="name-logo" src="/img/prestashop-logo-1524826491.jpg"></img></h2>
	  <div class="col-md-4">
	  	<a href="https://autonomoo.es/"><img class="foto-logo" src="/img/autonomoo.jpg"></img></a>
	  </div>
	  <div class="col-md-4">
		  <a href="http://www.elautonomodigital.es/"><img class="foto-logo" src="/img/autonomodigital.png"></img></a>
	  </div>
	  <div class="col-md-4">
		  <a href="https://tecnicoo.es/"><img class="foto-logo" src="/img/tecnicoo-logo.jpg"></img></a>
	  </div>
    <!--{block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}-->
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="col-md-8">
		<p>Mil gestiones</p>
		<p>Pasaje Doctor Serra, 2 - 10 </p>
		<p>46004 Valencia</p>
		<p>info@milgestiones.es</p>
      <!--{block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}-->
    </div>
    <div class="col-md-4">
		<img class="foto-logo" src="/img/100gestiones-footer.jpg"></img>
      <!--{block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}-->
    </div>
    <div class="row">
      <div class="col-md-12 footer-final">
        <p>
			<div class="col-md-6">
			  {block name='copyright_link'}
				<a class="_blank" href="http://autonomoo.es" target="_blank">
				  Powered by: Autonomoo
				</a>
			</div>
		  	<div class="col-md-6">
			<a class="terms" href="https://milgestiones.es/content/2-aviso-legal">Aviso Legal</a> - 
			<a class="terms" href="https://milgestiones.es/content/3-terminos-y-condiciones-de-uso">Términos y condiciones</a> - 
			<a class="terms" href="https://milgestiones.es/content/7-politica-privacidad">Política de privacidad</a>
          {/block}
		  </div>
        </p>
      </div>
    </div>
  </div>
</div>
