<?php /* Smarty version Smarty-3.1.19, created on 2018-05-03 14:25:08
         compiled from "/home/milgestiqx/www/modules/kbcustomfield/views/templates/admin/kb_field_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20132530025aeaffa439ad80-34598236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6cfc89e4f27551ed05cce348d00c024fcc14dee' => 
    array (
      0 => '/home/milgestiqx/www/modules/kbcustomfield/views/templates/admin/kb_field_form.tpl',
      1 => 1525350152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20132530025aeaffa439ad80-34598236',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'kb_form_contents' => 0,
    'edit_field_form' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5aeaffa44149a2_64179656',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aeaffa44149a2_64179656')) {function content_5aeaffa44149a2_64179656($_smarty_tpl) {?><div class="row">
    <div class="col-sm-12">
            
        <div class="panel">
            <div class="panel form-horizontal kb_custom_field_type">
        </div>
            <div class="kb_custom_field_form">
                <?php echo $_smarty_tpl->tpl_vars['kb_form_contents']->value;?>

            </div>
            
        </div>
    </div>
</div>    
<script>
    edit_field_form = <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['edit_field_form']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
;
    var kb_numeric = "<?php echo smartyTranslate(array('s'=>'Field should be numeric.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
";
    var kb_positive = "<?php echo smartyTranslate(array('s'=>'Field should be positive.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
";
    var maximum_length_excced = "<?php echo smartyTranslate(array('s'=>'Maximum length should be greater than minimum length.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
";
    var feature_not_available = "<?php echo smartyTranslate(array('s'=>'This feature is not available in free version.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
";
    velovalidation.setErrorLanguage({
        alphanumeric: "<?php echo smartyTranslate(array('s'=>'Field should be alphanumeric.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        digit_pass: "<?php echo smartyTranslate(array('s'=>'Password should contain atleast 1 digit.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        empty_field: "<?php echo smartyTranslate(array('s'=>'Field cannot be empty.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        number_field: "<?php echo smartyTranslate(array('s'=>'You can enter only numbers.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",            
        positive_number: "<?php echo smartyTranslate(array('s'=>'Number should be greater than 0.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        maxchar_field: "<?php echo smartyTranslate(array('s'=>'Field cannot be greater than # characters.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        minchar_field: "<?php echo smartyTranslate(array('s'=>'Field cannot be less than # character(s).','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        invalid_date: "<?php echo smartyTranslate(array('s'=>'Invalid date format.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        valid_amount: "<?php echo smartyTranslate(array('s'=>'Field should be numeric.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        valid_decimal: "<?php echo smartyTranslate(array('s'=>'Field can have only upto two decimal values.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        maxchar_size: "<?php echo smartyTranslate(array('s'=>'Size cannot be greater than # characters.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            specialchar_size: "<?php echo smartyTranslate(array('s'=>'Size should not have special characters.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            maxchar_bar: "<?php echo smartyTranslate(array('s'=>'Barcode cannot be greater than # characters.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            positive_amount: "<?php echo smartyTranslate(array('s'=>'Field should be positive.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            maxchar_color: "<?php echo smartyTranslate(array('s'=>'Color could not be greater than # characters.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            invalid_color: "<?php echo smartyTranslate(array('s'=>'Color is not valid.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            specialchar: "<?php echo smartyTranslate(array('s'=>'Special characters are not allowed.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            script: "<?php echo smartyTranslate(array('s'=>'Script tags are not allowed.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            style: "<?php echo smartyTranslate(array('s'=>'Style tags are not allowed.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            iframe: "<?php echo smartyTranslate(array('s'=>'Iframe tags are not allowed.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            image_size: "<?php echo smartyTranslate(array('s'=>'Uploaded file size must be less than #.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            html_tags: "<?php echo smartyTranslate(array('s'=>'Field should not contain HTML tags.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
            number_pos: "<?php echo smartyTranslate(array('s'=>'You can enter only positive numbers.','mod'=>'kbcustomfield'),$_smarty_tpl);?>
",
        });
</script>
<?php }} ?>
