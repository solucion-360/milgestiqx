<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_406e73c88a10a83d3eb3f80628e03dfe2f190986d860cd22f14df7a44f3f800b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_205b8c26e0df243b5881f6d9e440f75d2cd6a531ae728095c86486b44eb137cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_205b8c26e0df243b5881f6d9e440f75d2cd6a531ae728095c86486b44eb137cb->enter($__internal_205b8c26e0df243b5881f6d9e440f75d2cd6a531ae728095c86486b44eb137cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_205b8c26e0df243b5881f6d9e440f75d2cd6a531ae728095c86486b44eb137cb->leave($__internal_205b8c26e0df243b5881f6d9e440f75d2cd6a531ae728095c86486b44eb137cb_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_073523e6bf2976cdc9c207268366e9bacf4b52a0a44471627bf24a36f1b4584a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_073523e6bf2976cdc9c207268366e9bacf4b52a0a44471627bf24a36f1b4584a->enter($__internal_073523e6bf2976cdc9c207268366e9bacf4b52a0a44471627bf24a36f1b4584a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_073523e6bf2976cdc9c207268366e9bacf4b52a0a44471627bf24a36f1b4584a->leave($__internal_073523e6bf2976cdc9c207268366e9bacf4b52a0a44471627bf24a36f1b4584a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_afd675b7c49fe392d6500380b57793961769018e1ca92d622431d3243fc97bf1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afd675b7c49fe392d6500380b57793961769018e1ca92d622431d3243fc97bf1->enter($__internal_afd675b7c49fe392d6500380b57793961769018e1ca92d622431d3243fc97bf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_afd675b7c49fe392d6500380b57793961769018e1ca92d622431d3243fc97bf1->leave($__internal_afd675b7c49fe392d6500380b57793961769018e1ca92d622431d3243fc97bf1_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_021b2e6a17f0669a3619e95df77513b2e8d65d83266936c542133b8ba5416c8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_021b2e6a17f0669a3619e95df77513b2e8d65d83266936c542133b8ba5416c8e->enter($__internal_021b2e6a17f0669a3619e95df77513b2e8d65d83266936c542133b8ba5416c8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_021b2e6a17f0669a3619e95df77513b2e8d65d83266936c542133b8ba5416c8e->leave($__internal_021b2e6a17f0669a3619e95df77513b2e8d65d83266936c542133b8ba5416c8e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/milgestiqx/www/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
