<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_9b2a37554fb153be3234dce987523153cc3866dbbca78e66b518f23104930b92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da93bad7ff40aa446426bd05689b1421fcaf2c7e236d3d9affa74976bc4b33ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da93bad7ff40aa446426bd05689b1421fcaf2c7e236d3d9affa74976bc4b33ce->enter($__internal_da93bad7ff40aa446426bd05689b1421fcaf2c7e236d3d9affa74976bc4b33ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_da93bad7ff40aa446426bd05689b1421fcaf2c7e236d3d9affa74976bc4b33ce->leave($__internal_da93bad7ff40aa446426bd05689b1421fcaf2c7e236d3d9affa74976bc4b33ce_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_ab1ef0c0d0baef7eb0929b375bbb0d530c9c619a42760a518be6f2d3dd723d74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab1ef0c0d0baef7eb0929b375bbb0d530c9c619a42760a518be6f2d3dd723d74->enter($__internal_ab1ef0c0d0baef7eb0929b375bbb0d530c9c619a42760a518be6f2d3dd723d74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_ab1ef0c0d0baef7eb0929b375bbb0d530c9c619a42760a518be6f2d3dd723d74->leave($__internal_ab1ef0c0d0baef7eb0929b375bbb0d530c9c619a42760a518be6f2d3dd723d74_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_825a175f3c3505210e8ccf59fb2b48544b4083872d8331ebf2afd7ac44fd21f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_825a175f3c3505210e8ccf59fb2b48544b4083872d8331ebf2afd7ac44fd21f5->enter($__internal_825a175f3c3505210e8ccf59fb2b48544b4083872d8331ebf2afd7ac44fd21f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_825a175f3c3505210e8ccf59fb2b48544b4083872d8331ebf2afd7ac44fd21f5->leave($__internal_825a175f3c3505210e8ccf59fb2b48544b4083872d8331ebf2afd7ac44fd21f5_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_b9d5b80104e4cbbdaa2d04ef3d705bc809091572d86efcda434e47152ad82950 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9d5b80104e4cbbdaa2d04ef3d705bc809091572d86efcda434e47152ad82950->enter($__internal_b9d5b80104e4cbbdaa2d04ef3d705bc809091572d86efcda434e47152ad82950_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_b9d5b80104e4cbbdaa2d04ef3d705bc809091572d86efcda434e47152ad82950->leave($__internal_b9d5b80104e4cbbdaa2d04ef3d705bc809091572d86efcda434e47152ad82950_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/milgestiqx/www/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
